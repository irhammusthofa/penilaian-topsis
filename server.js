var socket  = require( 'socket.io' );
var express = require('express');
var app     = express();
var server  = require('http').createServer(app);
var io      = socket.listen( server );
var port    = process.env.PORT || 1500;

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});


io.on('connection', function (socket) {

  /*socket.on( 'new_count_message', function( data ) {
    io.sockets.emit( 'new_count_message', { 
    	new_count_message: data.new_count_message

    });
  });

  socket.on( 'update_count_message', function( data ) {
    io.sockets.emit( 'update_count_message', {
    	update_count_message: data.update_count_message 
    });
  });*/
  socket.on( 'login', function( data ) {
    console.log(data);
  });
  socket.on( 'location', function( data ) {
    io.sockets.emit( 'new_location', {
      lat: data.lat,
      lon: data.lon,
      last: data.last,
      id: data.id,
      nickname:data.nickname,
      nama: data.nama
    });
  });
  socket.on( 'delete', function( data ) {
    io.sockets.emit( 'delete_location', {
      id: data.id
    });
  });
  socket.on( 'new_message', function( data ) {
    io.sockets.emit( 'new_message', {
    	lat: data.name,
    	lon: data.address,
    	id: data.id
    });
  });

  
});
