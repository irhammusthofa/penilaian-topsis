<?php
/**
* Login
*/
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('logged_in'))
		{
			$role = $this->session->userdata('role');
			if ($role == 1){
				redirect('dasbor');
			}else{
				$this->session->set_flashdata("notification",notif_alert("Maaf, Role tidak ditemukan, silahkan login kembali.","danger"));
				redirect('logout');
			}
		}
		$this->load->model('model_user');
	}
	public function index(){
		$param = array(
			'title' => 'Login', 
		);
		$this->load->view('login',$param);
	}
	public function dologin(){
		$data['u_id']		= $this->input->post('username','true');
		$data['u_password']	= $this->input->post('password','true');

		$result = $this->model_user->login($data)->row();
		if (count($result)>0){
			$arr_login = array(
				'logged_in' => true,
				'role' 		=> $result->u_role,
				'username' 	=> $result->u_id);
			$this->session->set_userdata($arr_login);
		}else{
			$this->session->set_flashdata("notification",notif_alert("Maaf, Username atau Password salah.".pwd_hash($data['u_password']),"danger"));
		}
		redirect('login');
	}
}