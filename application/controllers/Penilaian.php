<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Penilaian
*/
class Penilaian extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_karyawan');
		$this->load->model('model_nilai');
	}
	public function mingguan(){
		$css 			= array(
			'assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
			'assets/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
		);

		$script = '
				
				var table = $(\'#table1\').DataTable({
				      "paging": true,
				      "lengthChange": true,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": false,
				    });
				';

		$js_footer		= array(
				array('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js',false,''),
				array('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',false,''),
				array('assets/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',false,''),

				array('',true,$script),
		);
		$data['karyawan']		= $this->model_karyawan->select_all()->result();
		$assets['js_footer']	= $js_footer; 
		$assets['css']			= $css; 
		$param = array(
			'title' 	=> 'Penilaian Mingguan',
			'content' 	=> 'penilaian/list_mingguan',
			'active'	=> 'penilaian-mingguan',
			'data'		=> $data,
			'assets'	=> $assets,
		);
		$this->load->view('layout_main',$param);
	}
	public function topsis(){
		$css 			= array(
			'assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
			'assets/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
		);

		$script = '
				
				var table = $(\'#table1\').DataTable({
				      "paging": true,
				      "lengthChange": true,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": false,
				    });
				function btnHapus(id)
			    {
			    	$("#btnYesDel").attr("href","'.base_url("karyawan/hapus").'/"+encodeURI(id));
			    	$("#desk_nta_del").html(id);
			    	$("#modalHapus").modal();
			    }
			    
				';

		$js_footer		= array(
				array('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js',false,''),
				array('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',false,''),
				array('assets/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',false,''),

				array('',true,$script),
		);
		$modal = array(
			array(
				'id' 	=> 'modalHapus',
				'title'	=> 'Hapus Karyawan',
				'type' 	=> 'delete',
				'body' 	=> 'Apakah anda yakin akan menghapus data berikut ?<br><br> NIK : <label id="desk_nta_del"></label><br><br> Nama : <label id="desk_nama_del"></label><br></label>',
				),
			);
		$data['karyawan']		= $this->model_karyawan->select_all()->result();
		$assets['js_footer']	= $js_footer; 
		$assets['css']			= $css; 
		$param = array(
			'title' 	=> 'Penilaian Topsis',
			'content' 	=> 'penilaian/list_topsis',
			'active'	=> 'penilaian-topsis',
			'data'		=> $data,
			'assets'	=> $assets,
		);
		$this->load->view('layout_main',$param);
	}
	public function edit($nik,$bln,$thn){
		$nik = urldecode($nik);
		$bln = urldecode($bln);
		$thn = urldecode($thn);
		$data['karyawan'] 			= $this->model_karyawan->by_nik($nik)->row();
		if (count($data['karyawan'])<=0) show_error('Karyawan dengan NIK '.$nik.' tidak ditemukan.','404');
		
		$data['kriteria'] 	= $this->model_nilai->list_kriteria()->result();

		$selected['nik'] = $nik;
		$selected['bln'] = $bln;
		$selected['thn'] = $thn;
		$param = array(
			'title' 	=> 'Penilaian Topsis',
			'content' 	=> 'penilaian/edit_topsis',
			'active'	=> 'penilaian-topsis',
			'selected'	=> $selected,
			'data'		=> $data,
		);
		$this->load->view('layout_main',$param);
	}
	public function edit_mingguan($nik,$bln,$thn){
		$nik = urldecode($nik);
		$bln = urldecode($bln);
		$thn = urldecode($thn);
		$select_kriteria 	= $this->input->get('kriteria','true');
		$select_minggu 		= $this->input->get('minggu','true');
		$data['karyawan'] 			= $this->model_karyawan->by_nik($nik)->row();
		$kriteria			= $this->model_nilai->list_kriteria()->result();
		if (count($data['karyawan'])<=0) show_error('Karyawan dengan NIK '.$nik.' tidak ditemukan.','404');
		$data['kriteria'] 	='';
		foreach ($kriteria as $val) {
			if (empty($select_kriteria)){
				$select_kriteria = $val->kode;
			}
			$data['kriteria'][$val->kode] = $val->kriteria;
		}
		$data['minggu'] 		= ['1'=>'1','2'=>'2','3'=>'3','4'=>'4'];
		if (empty($select_minggu)){
			$selected['minggu'] = 1;
		}else{
			$selected['minggu'] = $select_minggu;
		}

		$selected['kriteria'] 	= $select_kriteria;
		$data['sub_kriteria'] 	= $this->model_nilai->list_sub_kriteria($selected['kriteria'])->result();
		$data['rata2'] 			= $this->model_nilai->rata2_kriteria_mingguan($nik,$bln,$thn,$selected['minggu']);

		
		$selected['nik'] = $nik;
		$selected['bln'] = $bln;
		$selected['thn'] = $thn;
		$param = array(
			'title' 	=> 'Penilaian Mingguan',
			'content' 	=> 'penilaian/edit_mingguan',
			'active'	=> 'penilaian-mingguan',
			'selected'	=> $selected,
			'data'		=> $data,
		);
		$this->load->view('layout_main',$param);
	}
	public function detail($nik,$bln,$thn){
		$nik = urldecode($nik);
		$bln = urldecode($bln);
		$thn = urldecode($thn);
		$data['karyawan'] 			= $this->model_karyawan->by_nik($nik)->row();
		$data['kriteria-reference'] = $this->model_nilai->kriteria_reference($bln,$thn);
		$data['ternormalisasi'] 	= $this->model_nilai->ternormalisasi($data['kriteria-reference'],$bln,$thn);
		$data['ideal'] 				= $this->model_nilai->ideal($data['kriteria-reference'],$data['ternormalisasi']);
		$selected['nik'] = $nik;
		$selected['bln'] = $bln;
		$selected['thn'] = $thn;
		$param = array(
			'title' 	=> 'Penilaian Topsis',
			'content' 	=> 'penilaian/detail_topsis',
			'active'	=> 'penilaian-topsis',
			'selected'	=> $selected,
			'data'		=> $data,
		);
		$this->load->view('layout_main',$param);
	}
	public function hasil($bln,$thn){
		$bln = urldecode($bln);
		$thn = urldecode($thn);
		$data['karyawan'] 			= $this->model_karyawan->select_all()->result();
		$data['kriteria-reference'] = $this->model_nilai->kriteria_reference($bln,$thn);
		$data['ternormalisasi'] 	= $this->model_nilai->ternormalisasi($data['kriteria-reference'],$bln,$thn);
		$data['ideal'] 				= $this->model_nilai->ideal($data['kriteria-reference'],$data['ternormalisasi']);
		$selected['bln'] = $bln;
		$selected['thn'] = $thn;
		$param = array(
			'title' 	=> 'Penilaian Topsis',
			'content' 	=> 'penilaian/hasil',
			'active'	=> 'penilaian-topsis',
			'selected'	=> $selected,
			'data'		=> $data,
		);
		$this->load->view('layout_main',$param);
	}
	public function simpan($nik,$bln,$thn,$jenis=0,$redirect=''){
		$nik = urldecode($nik);
		$bln = urldecode($bln);
		$thn = urldecode($thn);

		$kriteria = $this->model_nilai->list_kriteria()->result();
		foreach ($kriteria as $row) {
			$data['id_karyawan'] 	= $nik;
			$data['kriteria'] 		= $row->kode;
			$data['nilai'] 			= $this->input->post('nilai['.$row->kode.']','true');
			$data['jenis'] 			= $jenis;
			$data['bln_thn'] 		= $bln.'-'.$thn;

			$data_nilai				= $this->model_nilai->get_nilai_nik_sub($nik,$row->kode,$jenis)->row();
			
			
			if (count($data_nilai)>0){
				$this->model_nilai->update($data_nilai->id,$data);
			}else{
				$this->model_nilai->insert($data);
			}
		}
		$this->session->set_flashdata("notification",notif_alert('Data nilai berhasil disimpan',"success"));
		if (empty($redirect)){
			redirect('penilaian/edit/'.$nik.'/'.$bln.'/'.$thn);
		}else{
			redirect('penilaian/edit_mingguan/'.$nik.'/'.$bln.'/'.$thn.'?minggu='.$jenis);
		}
	}
	public function cetak($bln,$thn){
		$bln = urldecode($bln);
		$thn = urldecode($thn);
		$data['karyawan'] 			= $this->model_karyawan->select_all()->result();
		$data['kriteria-reference'] = $this->model_nilai->kriteria_reference($bln,$thn);
		$data['ternormalisasi'] 	= $this->model_nilai->ternormalisasi($data['kriteria-reference'],$bln,$thn);
		$data['ideal'] 				= $this->model_nilai->ideal($data['kriteria-reference'],$data['ternormalisasi']);
		$selected['bln'] = $bln;
		$selected['thn'] = $thn;
		$param = array(
			'title' 	=> 'Penilaian Topsis',
			'active'	=> 'penilaian-topsis',
			'bln'		=> convert_bulan($bln),
			'thn'		=> $thn,
			'selected'	=> $selected,
			'data'		=> $data,
		);
		$this->load->view('penilaian/cetak',$param);
	}
	public function rangking($bln,$thn){
		$bln = urldecode($bln);
		$thn = urldecode($thn);
		$data['karyawan'] 			= $this->model_karyawan->select_all()->result();
		$data['kriteria-reference'] = $this->model_nilai->kriteria_reference($bln,$thn);
		$data['ternormalisasi'] 	= $this->model_nilai->ternormalisasi($data['kriteria-reference'],$bln,$thn);
		$data['ideal'] 				= $this->model_nilai->ideal($data['kriteria-reference'],$data['ternormalisasi']);
		$selected['bln'] = $bln;
		$selected['thn'] = $thn;
		$param = array(
			'title' 	=> 'Penilaian Topsis',
			'active'	=> 'penilaian-topsis',
			'bln'		=> convert_bulan($bln),
			'thn'		=> $thn,
			'selected'	=> $selected,
			'data'		=> $data,
		);
		$this->load->view('penilaian/rangking',$param);
	}
}