<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Akun
*/
class Akun extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		if ($this->profile->u_role !='ADMIN' && $this->profile->u_role!="CABANG"){
			show_error('Tidak memiliki hak akses.','403','Tidak diizinkan');
		}	
		$this->load->model('model_user');

	}
	public function profil(){
		$param = array(
			'title' 	=> 'Profil',
			'content' 	=> 'akun',
		);
		$this->load->view('layout_main',$param);
	}
	public function update($id){

		$data['u_id'] 			= $this->input->post('username','true');
		$data['u_name'] 		= $this->input->post('nama','true');
		
		$password 				= $this->input->post('password','true');
		$data['u_email'] 		= $this->input->post('email','true');
		if (!empty($password)){
			$data['u_password'] = pwd_hash($password);
		}
		$notif_success = "Akun anda berhasil diupdate";
		$notif_error = "Akun anda gagal diupadte.";

		$result = $this->model_user->update($id,$data);
		if ($result)
		{
			$this->session->set_flashdata("notification",notif_alert($notif_success,"info"));
		}else{
			$this->session->set_flashdata("notification",notif_alert($notif_error,"danger"));
		}
		redirect('akun/profil');
	}

}