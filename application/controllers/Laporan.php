<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Laporan
*/
class Laporan extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_laporan');
	}
	public function index(){
		$bln = $this->input->get('bln','true');
		$thn = $this->input->get('thn','true');
		$css 			= array(
			'assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
			'assets/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
		);

		$script = '
				
				var table = $(\'#table1\').DataTable({
				      "paging": true,
				      "lengthChange": true,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": false,
				    });
				function btnHapus(id)
			    {
			    	$("#btnYesDel").attr("href","'.base_url("karyawan/hapus").'/"+encodeURI(id));
			    	$("#desk_nta_del").html(id);
			    	$("#modalHapus").modal();
			    }
			    
				';

		$js_footer		= array(
				array('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js',false,''),
				array('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',false,''),
				array('assets/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',false,''),

				array('',true,$script),
		);
		$modal = array(
			array(
				'id' 	=> 'modalHapus',
				'title'	=> 'Hapus Laporan',
				'type' 	=> 'delete',
				'body' 	=> 'Apakah anda yakin akan menghapus data berikut ?<br><br> Id : <label id="desk_nta_del"></label><br></label>',
				),
			);
		$data['laporan']		= $this->model_laporan->select_all()->result();
		$assets['js_footer']	= $js_footer; 
		$assets['css']			= $css; 
		$param = array(
			'title' 	=> 'Penilaian Topsis',
			'content' 	=> 'penilaian/list_topsis',
			'active'	=> 'penilaian-topsis',
			'data'		=> $data,
			'assets'	=> $assets,
		);
		$this->load->view('layout_main',$param);
	}
}