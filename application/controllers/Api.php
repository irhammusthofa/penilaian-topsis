<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("model_karyawan");
	}
	public function login()
	{	
		$nik = $this->input->get("nik");
		$password = $this->input->get("password");

		if(empty($nik)){
			echo json_encode(array("status"=> FALSE,"message"=>"NIK tidak boleh kosong"));
		}else if(empty($password)){
			echo json_encode(array("status"=> FALSE,"message"=>"Password tidak boleh kosong"));
		}else{
			$login = $this->model_karyawan->login($nik,$password);
			if ($login!=FALSE){
				echo json_encode(array("status"=>TRUE,"message"=>"Login berhasil.","nama"=>$login->nama,"nickname"=>$login->username));
			}else{
				echo json_encode(array("status"=>FALSE,"message"=>"Login gagal, NIK atau Password salah."));
			}
			
		}
	}
}
