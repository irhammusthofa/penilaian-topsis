<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Karyawan
*/
class Karyawan extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_karyawan');
	}
	public function index(){
		$css 			= array(
			'assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
			'assets/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
		);

		$script = '
				
				var table = $(\'#table1\').DataTable({
				      "paging": true,
				      "lengthChange": true,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": false,
				    });
				function btnHapus(id,nama)
			    {
			    	$("#btnYesDel").attr("href","'.base_url("index.php/karyawan/hapus").'/"+encodeURI(id));
			    	$("#desk_nta_del").html(id);
			    	$("#desk_nama_del").html(decodeURI(nama));
			    	$("#modalHapus").modal();
			    }
			    
				';

		$js_footer		= array(
				array('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js',false,''),
				array('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',false,''),
				array('assets/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',false,''),

				array('',true,$script),
		);
		$modal = array(
			array(
				'id' 	=> 'modalHapus',
				'title'	=> 'Hapus Karyawan',
				'type' 	=> 'delete',
				'body' 	=> 'Apakah anda yakin akan menghapus data berikut ?<br><br> NIK : <label id="desk_nta_del"></label><br><br> Nama : <label id="desk_nama_del"></label><br></label>',
				),
			);
		$data['karyawan']		= $this->model_karyawan->select_all()->result();
		$assets['js_footer']	= $js_footer; 
		$assets['css']			= $css; 
		$assets['modal']		= $modal; 
		$param = array(
			'title' 	=> 'Data Karyawan',
			'content' 	=> 'karyawan/list',
			'active'	=> 'karyawan',
			'data'		=> $data,
			'assets'	=> $assets,
		);
		$this->load->view('layout_main',$param);
	}
	public function tambah(){
		$param = array(
			'title' 	=> 'Tambah Karyawan',
			'content' 	=> 'karyawan/form',
			'active'	=> 'karyawan',
			'action'	=> 'karyawan/simpan'
		);
		$this->load->view('layout_main',$param);
	}
	public function edit($id){
		$data['karyawan'] = $this->model_karyawan->by_nik($id)->row();
		$tmp['nik']				= $data['karyawan']->nik;
		$tmp['nama']			= $data['karyawan']->nama;
		$tmp['kelamin']			= $data['karyawan']->kelamin;
		$tmp['telp']			= $data['karyawan']->telp;
		$tmp['email']			= $data['karyawan']->email;
		$tmp['alamat']			= $data['karyawan']->alamat;
		$tmp['jabatan']			= $data['karyawan']->jabatan;
		$tmp['username']		= $data['karyawan']->username;
		$this->session->set_flashdata("temp2",$tmp);
		$param = array(
			'title' 	=> 'Edit Karyawan',
			'content' 	=> 'karyawan/form',
			'active'	=> 'karyawan',
			'data'		=> $data,
			'action'	=> 'karyawan/simpan'
		);
		$this->load->view('layout_main',$param);
	}
	public function simpan($id=''){
		if (empty($id) || $id==NULL){
			$redirect = 'tambah';
		}else{
			$redirect = 'edit';
		}
		$this->form_validation->set_rules('nik','NIK','required');
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required');
		$this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('username','Nickname','required');
		if ($redirect=='tambah') $this->form_validation->set_rules('password','password','required');

		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata("notification",notif_alert(validation_errors(),"danger"));
			redirect('karyawan/'.$redirect);
		}else{
			$data['nik']				= $this->input->post('nik');
			$data['nama']				= $this->input->post('nama');
			$data['kelamin']			= $this->input->post('jenis_kelamin');
			$data['telp']				= $this->input->post('telp');
			$data['email']				= $this->input->post('email');
			$data['alamat']				= $this->input->post('alamat');
			$data['jabatan']			= $this->input->post('jabatan');
			$data['username']			= $this->input->post('username');
			if ($redirect=='tambah'){
				$data['password']			= sha1($this->input->post('password'));
			}else{
				if (empty($this->input->post('password')) || $this->input->post('password')===NULL){

				}else{
					$data['password']			= sha1($this->input->post('password'));
				}
			}
			if ($redirect=='tambah'){
				$data['token']				= sha1($data['nik'].date('Y-m-d H:i:s'));
				$data['icon_map']			= sha1(date('Y-m-d H:i:s').$data['nik']);
				$this->session->set_flashdata("temp",$data);
				$namafile 					= $data['icon_map'].".png";
				$config['upload_path'] 		= './assets/icon-map/';
		        $config['allowed_types'] 	= 'jpg|png';
		        $config['max_size'] 		= '1024';
		        $config['file_name'] 		= $namafile;
		        $config['overwrite']		= true;
		        
		        $this->load->library('upload', $config);
		        if ($this->upload->do_upload('foto')){
		        	$data['icon_map'] = $config['file_name'];
		        	$filename 		= $this->upload->data();
					$query = $this->model_karyawan->insert($data);
					if ($query){
						$this->session->set_flashdata("notification",notif_alert('Karyawan berhasil ditambahkan',"success"));
						redirect('karyawan');
					}else{
						$this->session->set_flashdata("notification",notif_alert('Karyawan gagal ditambahkan',"danger"));
						redirect('karyawan/'.$redirect);
					}
		        }else{
		        	$msg		= $this->upload->display_errors();
		        	$msg_type	= 'danger';
		        }
			}else{
				if($_FILES['foto']['error']<=0){
					$karyawan 					= $this->model_karyawan->by_nik($data['nik'])->row();
					$data['icon_map']			= $karyawan->icon_map;
					$this->session->set_flashdata("temp",$data);
					$namafile 					= $data['icon_map'];
					$config['upload_path'] 		= './assets/icon-map/';
			        $config['allowed_types'] 	= 'jpg|png';
			        $config['max_size'] 		= '1024';
			        $config['file_name'] 		= $namafile;
			        $config['overwrite']		= true;
			        
			        $this->load->library('upload', $config);
			        if ($this->upload->do_upload('foto')){
			        	$data['icon_map'] = $config['file_name'];
			        	$filename 		= $this->upload->data();
						$query = $this->model_karyawan->update($id,$data);
						if ($query){
							$this->session->set_flashdata("notification",notif_alert('Karyawan berhasil diupdate',"success"));
							redirect('karyawan');
						}else{
							$this->session->set_flashdata("notification",notif_alert('Karyawan gagal diupdate',"danger"));
							redirect('karyawan/'.$redirect);
						}
			        }else{
			        	$msg		= $this->upload->display_errors();
			        	$msg_type	= 'danger';
			        }
				}else{
					$query = $this->model_karyawan->update($id,$data);
					if ($query){
						$this->session->set_flashdata("notification",notif_alert('Karyawan berhasil diupdate',"success"));
						redirect('karyawan');
					}else{
						$this->session->set_flashdata("notification",notif_alert('Karyawan gagal diupdate',"danger"));
						redirect('karyawan/'.$redirect);
					}
				}
			}

		}
		if (!empty($msg)){
	        	$this->session->set_flashdata('notification',notif_alert($msg,$msg_type));
	        }
	        
			redirect('karyawan/'.$redirect);
	}
	public function hapus($nik){
		$nik = urldecode($nik);
		$delete = $this->model_karyawan->delete(['nik'=>$nik]);
		if ($delete){
			$this->session->set_flashdata("notification",notif_alert('Karyawan berhasil dihapus',"success"));
			redirect('karyawan');
		}else{
			$this->session->set_flashdata("notification",notif_alert('Karyawan gagal dihapus',"danger"));
			redirect('karyawan');
		}
	}
}