<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Dasbor
*/
class Dasbor extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}
	public function index(){
		/**/
		$css 			= array();
		$script = '

				var map;
				var marker=[];
		 		var socket = io.connect("http://"+window.location.hostname+":1500" );
		 		setInterval(function(){
		 				var i;
						for(i =0;i<marker.length;i++){
							var d 	= new Date(marker[i]["last"]);
							var now = new Date();
							if (now>d){
								console.log(d);
								console.log(now);
								marker[i]["position"].setMap(null);
					      		marker.splice(i,1);
							} 
						}
						
					}, 2000);
				function someAsyncOperation() {
					const startCallback = Date.now();

					  // do something that will take 10ms...
					  socket.emit("request", {
					      request: "check_connection",
					    });
					  while (Date.now() - startCallback < 10) {
					    // do nothing
					    console.log("echo");
					  }
					
					//someAsyncOperation();
				}
				someAsyncOperation();
				  socket.on( "new_location", function( data ) {
				  	console.log(data);
				      if (marker.length==0){
				      	console.log("push, marker length = "+marker.length);
				      	var id = data.id;
				      	var nickname = data.nickname;
				      	var nama = data.nama;
				      	marker.push({id: id, position: null, last : data.last});
					      marker[marker.length-1]["position"] = new google.maps.Marker({
					          position: {lat:data.lat,lng:data.lon},
							  label: nickname,
					          map: map
					        });
						var infowindow = new google.maps.InfoWindow({
				          content: "<b style=\"color:black\">NIK : "+id+"</b><br><b style=\"color:black\">NAMA : "+nama+"</b>"
				        });
						marker[marker.length-1]["position"].addListener("click", function() {
				          infowindow.open(map, marker[marker.length-1]["position"]);
				        });
				      }else{
				      	var i,index;
				      	for(i=0;i<marker.length;i++){
				      		console.log("length " + marker.length);
				      		console.log("marker "+i+ ", " + marker[i].id);
				      		if (marker[i].id==data.id){
				      			index=i;
				      			break;
				      		}
				      	}
				      	if (index==undefined){
				      		console.log("index " + index);
				      		console.log("id " + data.id);
				      		var id = data.id;
				      		var nickname = data.nickname;
				      		var nama = data.nama;
				      		marker.push({id: id, position: null, last : null});
					      	marker[marker.length-1]["position"] = new google.maps.Marker({
					          position: {lat:data.lat,lng:data.lon},
							  label: nickname,
					          map: map
					        });
							var infowindow = new google.maps.InfoWindow({
					          content: "<b style=\"color:black\">NIK : "+id+"</b><br><b style=\"color:black\">NAMA : "+nama+"</b>"
					        });
							marker[marker.length-1]["position"].addListener("click", function() {
					          infowindow.open(map, marker[marker.length-1]["position"]);
					        });
							marker[marker.length-1]["last"] = data.last;
				      	}else{
				      		console.log("index " + index);
				      		var LatLng = new google.maps.LatLng(data.lat, data.lon);
				      		marker[index]["position"].setPosition(LatLng);
				      		marker[index]["last"] = data.last;
				      	}
				      }
				    });
				    socket.on( "delete_location", function( data ) {
					  	console.log(data);
					      if (marker.length==0){
					      	
					      }else{
					      	var i,index;
					      	for(i=0;i<marker.length;i++){
					      		console.log("marker "+i+ ", " + marker[i].id);
					      		if (marker[i].id==data.id){
					      			marker[i]["position"].setMap(null);
					      			marker.splice(i,1);
				      				console.log("length " + marker.length);
					      			break;
					      		}
					      	}
					      }
					    });
				function initMap() {
			        var uluru = {lat: -6.200962, lng: 106.650487};
			        map = new google.maps.Map(document.getElementById("world-map"), {
			          zoom: 13,
			          center: uluru
			        });
			        
			      }
			    
				';

		$js_footer		= array(
				array('',true,$script),
		);
		$assets['js_footer']	= $js_footer; 
		$assets['css']			= $css; 
		$param = array(
			'title' 	=> 'Dasbor',
			'content' 	=> 'dasbor_operator',
			'active'	=> 'dasbor',
			'assets'	=> $assets,
		);
		$this->load->view('layout_main',$param);
	}
}