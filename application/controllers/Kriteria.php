<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Kriteria
*/
class Kriteria extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_kriteria');
	}
	public function index(){
		$css 			= array(
			'assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
			'assets/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
		);

		$script = '
				
				var table = $(\'#table1\').DataTable({
				      "paging": true,
				      "lengthChange": true,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": false,
				    });
				function btnHapus(id,nama)
			    {
			    	$("#btnYesDel").attr("href","'.base_url("index.php/kriteria/hapus").'/"+encodeURI(id));
			    	$("#desk_nta_del").html(id);
			    	$("#desk_nama_del").html(decodeURI(nama));
			    	$("#modalHapus").modal();
			    }
			    
				';

		$js_footer		= array(
				array('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js',false,''),
				array('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',false,''),
				array('assets/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',false,''),

				array('',true,$script),
		);
		$modal = array(
			array(
				'id' 	=> 'modalHapus',
				'title'	=> 'Hapus Kriteria',
				'type' 	=> 'delete',
				'body' 	=> 'Apakah anda yakin akan menghapus data berikut ?<br><br> Kode : <label id="desk_nta_del"></label><br><br> Kriteria : <label id="desk_nama_del"></label><br></label>',
				),
			);
		$data['kriteria']		= $this->model_kriteria->select_all()->result();
		$assets['js_footer']	= $js_footer; 
		$assets['css']			= $css; 
		$assets['modal']		= $modal; 
		$param = array(
			'title' 	=> 'Data Kriteria',
			'content' 	=> 'kriteria/list',
			'active'	=> 'kriteria',
			'data'		=> $data,
			'assets'	=> $assets,
		);
		$this->load->view('layout_main',$param);
	}
	public function tambah(){
		$param = array(
			'title' 	=> 'Tambah Kriteria',
			'content' 	=> 'kriteria/form',
			'active'	=> 'kriteria',
			'action'	=> 'kriteria/simpan'
		);
		$this->load->view('layout_main',$param);
	}
	public function edit($id){
		$data['kriteria'] 		= $this->model_kriteria->by_kode($id)->row();
		$tmp['kode']				= $data['kriteria']->kode;
		$tmp['kriteria']			= $data['kriteria']->kriteria;
		$this->session->set_flashdata("temp2",$tmp);
		$param = array(
			'title' 	=> 'Edit Kriteria',
			'content' 	=> 'kriteria/form',
			'active'	=> 'kriteria',
			'data'		=> $data,
			'action'	=> 'kriteria/simpan'
		);
		$this->load->view('layout_main',$param);
	}
	public function simpan($id=''){
		if (empty($id) || $id==NULL){
			$redirect = 'tambah';
		}else{
			$redirect = 'edit';
		}
		$this->form_validation->set_rules('kode','Kode','required');
		$this->form_validation->set_rules('kriteria','Kriteria','required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata("notification",notif_alert(validation_errors(),"danger"));
			redirect('kriteria/'.$redirect);
		}else{
			$data['kode']				= $this->input->post('kode');
			$data['kriteria']			= $this->input->post('kriteria');
			
			if ($redirect=='tambah'){		
				$query = $this->model_kriteria->insert($data);
				if ($query){
					$this->session->set_flashdata("notification",notif_alert('Kriteria berhasil ditambahkan',"success"));
					redirect('kriteria');
				}else{
					$this->session->set_flashdata("notification",notif_alert('Kriteria gagal ditambahkan',"danger"));
					redirect('kriteria/'.$redirect);
				}
			}else{
				
				$query = $this->model_kriteria->update($id,$data);
				if ($query){
					$this->session->set_flashdata("notification",notif_alert('Kriteria berhasil diupdate',"success"));
					redirect('kriteria');
				}else{
					$this->session->set_flashdata("notification",notif_alert('Kriteria gagal diupdate',"danger"));
					redirect('kriteria/'.$redirect);
				}
			}

		}
		if (!empty($msg)){
	        	$this->session->set_flashdata('notification',notif_alert($msg,$msg_type));
	        }
	        
			redirect('kriteria/'.$redirect);
	}
	public function hapus($kode){
		$kode = urldecode($kode);
		$delete = $this->model_kriteria->delete(['kode'=>$kode]);
		if ($delete){
			$this->session->set_flashdata("notification",notif_alert('Kriteria berhasil dihapus',"success"));
			redirect('kriteria');
		}else{
			$this->session->set_flashdata("notification",notif_alert('Kriteria gagal dihapus',"danger"));
			redirect('kriteria');
		}
	}
}