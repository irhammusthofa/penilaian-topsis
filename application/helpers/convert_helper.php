<?php
function convert_nilai($nilai){
	switch ($nilai) {
		case '1': return 'Sangat Rendah'; 
			break;
		case '2': return 'Rendah'; 
			break;
		case '3': return 'Cukup'; 
			break;
		case '4': return 'Tinggi'; 
			break;
		case '5': return 'Sangat Tinggi'; 
			break;
		
		default:
			# code...
			break;
	}
}
function UR_exists($url){
   $headers=get_headers($url);
   return stripos($headers[0],"200 OK")?true:false;
}
function current_full_url()
{
    $CI =& get_instance();

    $url = $CI->config->site_url($CI->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}
function format_date($data){
	$date = explode("-", $data);

	return $date[2]."-".$date[0]."-".$date[1];
}
function convert_date($date){
	$tgl = date_format($date,"d");
	$bln = date_format($date,"m");
	$thn = date_format($date,"Y");

	return $tgl.' '.convert_bulan($bln).' '.$thn;
}
function convert_bulan($data)
{
	switch ($data) {
		case '1': return 'Januari';break;
		case '2': return 'Februari';break;
		case '3': return 'Maret';break;
		case '4': return 'April';break;
		case '5': return 'Mei';break;
		case '6': return 'Juni';break;
		case '7': return 'Juli';break;
		case '8': return 'Agustus';break;
		case '9': return 'September';break;
		case '10': return 'Oktober';break;
		case '11': return 'November';break;
		case '12': return 'Desember';break;
		default:
			return 'Error Convert Bulan';
			break;
	}
}

function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}


function status_user($data,$skin=false)
{
	$tmp = '<label class="label label-%type%">%label%</label>';
	switch ($data) {
		case 1:
			if (!$skin)
			{
				$tmp = 'AKTIF';
			}else{
				$tmp = str_replace('%label%', 'AKTIF', $tmp);
				$tmp = str_replace('%type%', 'success', $tmp);
			}
			break;
		case 2:
			if (!$skin)
			{
				$tmp = 'BLOKIR';
			}else{
				$tmp = str_replace('%label%', 'BLOKIR', $tmp);
				$tmp = str_replace('%type%', 'warning', $tmp);
			}
			break;
		default:
			if (!$skin)
			{
				$tmp = 'TIDAK AKTIF';
			}else{
				$tmp = str_replace('%label%', 'TIDAK AKTIF', $tmp);
				$tmp = str_replace('%type%', 'danger', $tmp);
			}
			break;
	}
	return $tmp;
}
function status_karyawan($data,$skin=false)
{
	$tmp = '<label class="label label-%type%">%label%</label>';
	switch ($data) {
		case 1:
			if (!$skin)
			{
				$tmp = 'AKTIF';
			}else{
				$tmp = str_replace('%label%', 'AKTIF', $tmp);
				$tmp = str_replace('%type%', 'success', $tmp);
			}
			break;
		case 2:
			if (!$skin)
			{
				$tmp = 'BLOKIR';
			}else{
				$tmp = str_replace('%label%', 'BLOKIR', $tmp);
				$tmp = str_replace('%type%', 'warning', $tmp);
			}
			break;
		default:
			if (!$skin)
			{
				$tmp = 'TIDAK AKTIF';
			}else{
				$tmp = str_replace('%label%', 'TIDAK AKTIF', $tmp);
				$tmp = str_replace('%type%', 'danger', $tmp);
			}
			break;
	}
	return $tmp;
}
function status_approve($data,$skin=false)
{
	$tmp = '<label class="label label-%type%">%label%</label>';
	switch ($data) {
		case 1:
			if (!$skin)
			{
				$tmp = 'Disetujui';
			}else{
				$tmp = str_replace('%label%', 'Disetujui', $tmp);
				$tmp = str_replace('%type%', 'success', $tmp);
			}
			break;
		case 2:
			if (!$skin)
			{
				$tmp = 'Ditolak';
			}else{
				$tmp = str_replace('%label%', 'Ditolak', $tmp);
				$tmp = str_replace('%type%', 'danger', $tmp);
			}
			break;
		default:
			if (!$skin)
			{
				$tmp = 'Belum disetujui';
			}else{
				$tmp = str_replace('%label%', 'Belum disetujui', $tmp);
				$tmp = str_replace('%type%', 'warning', $tmp);
			}
			break;
	}
	return $tmp;
}
function role($data){
	switch ($data) {
		case '1':
			return 'ADMIN';
			break;
		
		case '2':
			return 'OPERATOR';
			break;

		case '3':
			return 'PIMPINAN';
			break;

		case '4':
			return 'KARYAWAN';
			break;
		default:
			# code...
			break;
	}
}
function pwd_hash($data)
{
	return sha1("%$@3#".$val."()*&^55");
}