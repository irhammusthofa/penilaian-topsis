<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function notif_alert($message,$skin)
{
	return '<div class="alert alert-'.$skin.'">'.$message.'</div>';
}
function notif_alert_dark($message,$skin)
{
    return '<div class="alert alert-page pa_page_alerts_dark alert-'.$skin.' alert-dark" >'.$message.'</div>';
}
function label_alert($message,$skin)
{
    return '<span class="label label-'.$skin.'">'.$message.'</span>';
}