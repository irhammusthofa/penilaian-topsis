<?php

/* Example :
	$css 			= array(
			'assets/css/libs/footable.core.css',
			'assets/css/libs/select2.css',
	);
	$script = "$(document).ready(function() {
		$('.footable').footable();
	}); $('#sel2').select2();";
	$js_footer		= array(
			array('assets/js/footable.js',false,''),
			array('assets/js/footable.sort.js',false,''),
			array('assets/js/footable.paginate.js',false,''),
			array('assets/js/footable.filter.js',false,''),
			array('assets/js/select2.min.js',false,''),
			array('',true,$script),
	);
	
	$assets['js_footer']	= $js_footer; 
	$assets['css']	= $css; 
*/
function convert_css($css)
{
	$tmp = "";
	for ($i=0;$i<count($css);$i++) {
		$tmp = $tmp.'<link href="'.base_url($css[$i]).'" rel="stylesheet" />';		
	}
	return $tmp;
}
function convert_js($data)
{

	if (empty($data)){
		return '';	
	}else{
		$tmp = "";
		for ($i=0;$i<count($data);$i++) {
			if ($data[$i][1]==false){
				if (isset($data[$i][3])){
					if ($data[$i][3]==true){
						if (!substr($data[$i][0], 4)=='http')
						{
							$tmp = $tmp.'<script src="'.base_url($data[$i][0]).'"></script>';
						}else{
							$tmp = $tmp.'<script src="'.base_url($data[$i][0]).'"></script>';
						}
					}else{
						$tmp = $tmp.'<script src="'.$data[$i][0].'"></script>';
					}
				}else{
					if (!substr($data[$i][0], 4)=='http')
						{
							$tmp = $tmp.'<script src="'.base_url($data[$i][0]).'"></script>';
						}else{
							$tmp = $tmp.'<script src="'.base_url($data[$i][0]).'"></script>';
						}
					
				}
			}else{
				$tmp = $tmp.'<script type="text/javascript">'.$data[$i][2].'</script>';
			}	
		}

		return $tmp;
	}
}
function create_modal($modal)
{
	if (!empty($modal)){
		
		$template = '<div class="modal fade" id="{{id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">{{title}}</h4>
		      </div>
		      <div class="modal-body">
		        {{body}}
		      </div>
		      <div class="modal-footer">
		        {{button}}
		      </div>
		    </div>
		  </div>
		</div>';


		$CI =& get_instance();
		foreach ($modal as $val) {
			$tmp = $template;
			if ($val['type'] == 'delete')
			{
				if (empty(@$val['btnYesDel'])) $val['btnYesDel'] = 'btnYesDel';
				$btnYes 	= anchor('delete','Ya',array('id' => $val['btnYesDel'],'class' => 'btn btn-danger'));
				$btnCancel = '<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>';
				$button = $btnYes.$btnCancel;
				$button .= @$val['button'];
				$tmp = str_replace('{{body}}', @$val['body'], $tmp);
				$tmp = str_replace('{{button}}', @$button, $tmp);
			}else if ($val['type'] == 'reset')
			{
				if (empty(@$val['btnYesReset'])) $val['btnYesReset'] = 'btnYesReset';
				$btnYes 	= anchor('reset','Ya',array('id' => $val['btnYesReset'],'class' => 'btn btn-danger'));
				$btnCancel = '<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>';
				$button = $btnYes.$btnCancel;
				$button .= @$val['button'];
				$tmp = str_replace('{{body}}', @$val['body'], $tmp);
				$tmp = str_replace('{{button}}', @$button, $tmp);
			}else if ($val['type'] == 'approve')
			{
				if (empty(@$val['btnYesApprove'])) $val['btnYesApprove'] = 'btnYesApprove';
				$btnYes 	= anchor('appove','Ya',array('id' => $val['btnYesApprove'],'class' => 'btn btn-success'));
				$btnNo 		= anchor('appove','Tolak',array('id' => 'btnNoApprove','class' => 'btn btn-danger'));
				$btnCancel = '<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>';
				$button = $btnYes.$btnNo.$btnCancel;
				$button .= @$val['button'];
				$tmp = str_replace('{{body}}', @$val['body'], $tmp);
				$tmp = str_replace('{{button}}', @$button, $tmp);
			}else{
				if (empty(@$val["btnSimpan"])) $val["btnSimpan"] = 'btnSimpan';
				$view = $CI->load->view(@$val['body'],'',TRUE);
				$body = explode('<!--start-box-->', $view );
				$body = explode('<!--end-box-->', $body[1]);
				$form = form_open($val['form'],@$val['array_form']);
				$body = $form.$body[0].form_close();
				$tmp = str_replace('{{body}}',$body, $tmp);
				$btnYes = '<button id="'.$val["btnSimpan"].'" type="button" class="btn btn-primary">Simpan</button>';
				$btnCancel = '<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>';
				
				$button = $btnYes.$btnCancel;
				$tmp = str_replace('{{button}}', @$button, $tmp);
			}
				
				$tmp = str_replace('{{id}}', @$val['id'], $tmp);
				$tmp = str_replace('{{title}}', @$val['title'], $tmp);
			echo $tmp;
		}	
	}

}
