<?php
/**
* Model_kriteria
*/
class Model_kriteria extends CI_Model
{
	public function select_all(){
		return $this->db->select('*')
			->from('kriteria')
			->get();
	}
	public function by_kode($id){
		return $this->db->select('*')
			->from('kriteria')
			->where('kode',$id)
			->get();
	}
	public function insert($data){
		return $this->db->insert('kriteria',$data);
	}
	public function update($id,$data){
		$this->db->where('kode',$id);
		return $this->db->update('kriteria',$data);
	}
	public function delete($data){
		return $this->db->delete('kriteria',$data);
	}
}