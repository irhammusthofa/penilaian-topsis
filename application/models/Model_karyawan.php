<?php
/**
* Model_karyawan
*/
class Model_karyawan extends CI_Model
{
	public function select_all(){
		return $this->db->select('*')
			->from('karyawan')
			->get();
	}
	public function by_nik($nikd){
		return $this->db->select('*')
			->from('karyawan')
			->where('nik',$nikd)
			->get();
	}
	public function login($nik,$pass){
		$query = $this->db->select('*')
			->from('karyawan')
			->where('nik',$nik)
			->where('password',sha1($pass))
			->get()->row();
		if (count($query)>0){
			return $query;
		}
		return FALSE;
	}
	public function insert($data){
		return $this->db->insert('karyawan',$data);
	}

	public function update($id,$data){
		$this->db->where('nik',$id);
		return $this->db->update('karyawan',$data);
	}
	public function delete($data){
		return $this->db->delete('karyawan',$data);
	}
}