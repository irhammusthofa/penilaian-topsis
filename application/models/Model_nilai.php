<?php
/**
* Model_nilai
*/
class Model_nilai extends CI_Model
{
	public function list_kriteria(){
		return $this->db->select('*')
			->from('kriteria')
			->get();
	}
	public function list_sub_kriteria($kriteria=''){
		if (!empty($kriteria)){
			$this->db->where('k.kode',$kriteria);
		}
		return $this->db->select('*')
			->from('kriteria k')
			->join('sub_kriteria s','s.id_kriteria=k.kode','inner')
			->get();
	}
	public function rata2_kriteria_mingguan($nik,$bln,$thn,$minggu){
		$data_kriteria = $this->list_kriteria()->result();
		$nilai_kriteria = '';
		foreach ($data_kriteria as $val) {
			$count = count($this->list_sub_kriteria($val->kode)->result());
			$sum = $this->db->select('sum(nilai) AS nilai')
				->from('penilaian p')
				->join('sub_kriteria sb','sb.id=p.sub_kriteria','inner')
				->join('kriteria k','k.kode=sb.id_kriteria','inner')
				->where_in('p.jenis',$minggu)
				->where('p.bln_thn',$bln.'-'.$thn)
				->where('p.id_karyawan',$nik)
				->where('k.kode',$val->kode)
				->get()->row()->nilai;

			if ($sum==0){
				$nilai_kriteria += 0;
			}else{
				$nilai_kriteria+= $sum/$count;
			}
		}
		if ($nilai_kriteria==0) return 0;
		return $nilai_kriteria/count($data_kriteria);
	}
	public function rata2_kriteria($nik,$bln,$thn,$kriteria){
		//$count = count($this->list_sub_kriteria($kriteria)->result());
		$sum = $this->db->select('sum(nilai) AS nilai')
			->from('penilaian p')
			->join('kriteria k','k.kode=p.kriteria','inner')
			->where_in('p.jenis','0')
			->where('p.bln_thn',$bln.'-'.$thn)
			->where('p.id_karyawan',$nik)
			->where('k.kode',$kriteria)
			->get()->row()->nilai;
		//if ($sum==0) return 0;
		return $sum;
	}
	public function select_all($bln_thn,$jenis=[0]){
		return $this->db->select('*')
			->from('penilaian p')
			->join('karyawan k','k.nik=p.id_karyawan','inner')
			->where_in('p.jenis',$jenis)
			->where('p.bln_thn',$bln_thn)
			->get();
	}
	private function alternatif($month,$year){
		return $this->db->select('id_karyawan')
			->from('penilaian')
			->where('bln_thn',$month."-".$year)
			->where('jenis',0)
			->group_by('id_karyawan')
			->get();
	}
	private function get_bobot($kode,$month,$year){
		$karyawan 	= $this->model_karyawan->select_all()->result();
		$bobot 		= '';
		foreach ($karyawan as $row) {
			$rata2 = $this->rata2_kriteria($row->nik,$month,$year,$kode);
			$bobot[]	= array('nilai'=>$rata2);
		}
		$tmp = 0;
		foreach ($bobot as $val) {
			$tmp += $val['nilai'];
		}
		/*$bobot = $this->db->select('SUM(nilai) AS total')
			->from('penilaian p')
			->join('sub_kriteria sb','sb.id_kriteria=sb.id_kriteria','inner')
			->where('p.bln_thn',$month."-".$year)
			->where('p.jenis',0)
			->where('sb.id_kriteria',$kode)
			->get();*/
		if ($tmp==0) return 0;
		return $tmp/count($karyawan);
	}

	private function get_pembagi($kode,$month,$year){
		$karyawan 	= $this->model_karyawan->select_all()->result();
		$bobot 		= '';
		foreach ($karyawan as $row) {
			$rata2 = $this->rata2_kriteria($row->nik,$month,$year,$kode);
			$bobot[]	= array('nilai'=>$rata2);
		}
		$tmp = 0;
		foreach ($bobot as $val) {
			$tmp += ($val['nilai']*$val['nilai']);
		}
		return sqrt($tmp);
	}
	private function get_nilai($nik,$kode,$month,$year){
		$nilai 			= $this->rata2_kriteria($nik,$month,$year,$kode);
		if ($nilai==0) return 0;
		return $nilai;
	}
	public function rangking($nik,$month,$year){
		$array = $this->get_relative_closeness($month,$year);
		$rank 	= "";
		for($i=0;$i<count($array);$i++){
			$rank[] = array(
				'rc'			=> @$array[$i]['rc'],
				'id_karyawan'	=> @$array[$i]['id_karyawan'],
			);
		}
		arsort($rank);
		$i = 0;
		foreach ($rank as $val) {
			$i++;
			if ($val['id_karyawan']==$nik){
				return $i;
			}
		}
	}
	public function kriteria_reference($month,$year){
		$kriteria 		= $this->list_kriteria()->result();
		foreach ($kriteria as $val) {
			$bobot = $this->get_bobot($val->kode,$month,$year);
			$pembagi = $this->get_pembagi($val->kode,$month,$year);
			$kriteria_reference[] = array(
				'kode' 			=> $val->kode,
				'data'			=> array(
					'c/b'		=> ($bobot<2) ? 'COST' : 'BENEFIT',
					'bobot'		=> $bobot,
					'pembagi'	=> $pembagi,
				),
			);
		}
		return $kriteria_reference;
	}
	public function relative_closeness($nik,$month,$year){
		$array = $this->get_relative_closeness($month,$year);
		for($i=0;$i<count($array);$i++){
			if (@$array[$i]['id_karyawan']==$nik){
				$relative_closeness = array(
					'id_karyawan'	=> $nik,
					's_plus'		=> $array[$i]['s_plus'],
					's_min'			=> $array[$i]['s_min'],
					'rc'			=> $array[$i]['rc']
				);
				return $relative_closeness;
			}
		}
		$relative_closeness = array(
			'id_karyawan'	=> $nik,
			's_plus'		=> 0,
			's_min'			=> 0,
			'rc'			=> 0
		);
		return $relative_closeness;
	}
	public function ternormalisasi($kriteria_reference,$month,$year){

		$kriteria 		= $this->list_kriteria()->result();
		$alternatif 	= $this->alternatif($month,$year)->result();
		$alternatif_reference = '';
		foreach ($alternatif as $val) {
			$data = "";
			foreach ($kriteria as $val_k) {
				$nilai 				= 0;
				$ref_kriteria 		= $this->find_refkriteria($kriteria_reference,$val_k->kode);
				$pembagi 			= $ref_kriteria['data']['pembagi'];
				$nilai 				= $this->get_nilai($val->id_karyawan,$val_k->kode,$month,$year);
				$ternormalisasi 	= $nilai/$pembagi;
				if ($nilai!=0){
					$ternormalisasi 	= $nilai/$pembagi;	
				}else{
					$ternormalisasi 	= 0;
				}
				$bobot 				= $ref_kriteria['data']['bobot'];

				$alternatif_reference[] = array(
					'id_karyawan'	=> $val->id_karyawan,
					'kriteria'		=> $val_k->kode,
					'nilai'			=> $nilai,
					'ternormalisasi'=> $ternormalisasi,
					'terbobot' 		=> $ternormalisasi * $bobot,
				);
			}
		}
		return $alternatif_reference;
	}
	public function ideal($kriteria_reference,$alternatif_reference){
		$kriteria 		= $this->list_kriteria()->result();
		foreach ($kriteria as $val) {
			$ref_kriteria 		= $this->find_refkriteria($kriteria_reference,$val->kode);
			$cb 				= $ref_kriteria['data']['c/b'];
			$ref_terbobot		= $this->find_refterbobot($alternatif_reference,$val->kode,'kriteria');
			if ($cb=='BENEFIT'){
				$a_plus = max($ref_terbobot);
				$a_min 	= min($ref_terbobot);
			}else{
				$a_plus = min($ref_terbobot);
				$a_min 	= max($ref_terbobot);
			}
			$tab_ideal[] = array(
				'kriteria' 	=> $val->kode,
				'a_plus'	=> $a_plus,
				'a_min'		=> $a_min,
			);
		}
		return $tab_ideal;
	}
	private function get_relative_closeness($month,$year){
		$kriteria 		= $this->list_kriteria()->result();
		$count_kriteria = count($kriteria);
		$alternatif 	= $this->alternatif($month,$year)->result();
		$count_alternatif = count($alternatif);
		if ($count_alternatif<=0 || $count_kriteria <=0){
			$relative_closeness = array(
				's_plus'		=> 0,
				's_min'			=> 0,
				'rc'			=> 0
			);
			return $relative_closeness;
		}
		$kriteria_reference = $this->kriteria_reference($month,$year);
		$alternatif_reference = $this->ternormalisasi($kriteria_reference,$month,$year);
		
		$tab_ideal = $this->ideal($kriteria_reference,$alternatif_reference);
		
		$relative_closeness = "";
		foreach ($alternatif as $val) {
			$s_plus = 0;
			$s_min 	= 0;
			foreach ($tab_ideal as $tab) {
				$ref_terbobot		= $this->find_refterbobot_kriteria($alternatif_reference,$val->id_karyawan,'id_karyawan',$tab['kriteria']);
				$s_plus += ($tab['a_plus']-$ref_terbobot)*($tab['a_plus']-$ref_terbobot);
				$s_min += (($tab['a_min']-$ref_terbobot)*($tab['a_min']-$ref_terbobot));
			}
			$s_plus = sqrt($s_plus);
			$s_min = sqrt($s_min);

			$relative_closeness[] = array(
				'id_karyawan'	=> $val->id_karyawan,
				's_plus'		=> $s_plus,
				's_min'			=> $s_min,
				'rc'			=> ($s_min!=0) ? $s_min / ($s_min+$s_plus) : 0,
			);
			
		}
		return $relative_closeness;
		
	}
	private function find_refkriteria($source,$kriteria){
		for($i=0;$i<count($source);$i++){
			if ($kriteria==$source[$i]['kode']){
				return $source[$i];
			}
		}
		return FALSE;
	}
	private function find_refterbobot_kriteria($source,$id_karyawan,$key,$kriteria){
		$tmp = "";
		for($i=0;$i<count($source);$i++){
			if ($id_karyawan==$source[$i][$key] && $kriteria==$source[$i]['kriteria']){
				return $source[$i]['terbobot'];
			}
		}
		return 0;
	}
	private function find_refterbobot($source,$kriteria,$key){
		$tmp = "";
		for($i=0;$i<count($source);$i++){
			if ($kriteria==$source[$i][$key]){
				$tmp[] = $source[$i]['terbobot'];
			}
		}
		return $tmp;
	}
	public function insert($data){
		return $this->db->insert('penilaian',$data);
	}

	public function update($id,$data){
		$this->db->where('id',$id);
		return $this->db->update('penilaian',$data);
	}
	public function get_nilai_nik_sub($nik,$sub,$jenis=0){
		return $this->db->select('*')
			->from('penilaian')
			->where('id_karyawan',$nik)
			->where('kriteria',$sub)
			->where('jenis',$jenis)
			->get();
	}
}