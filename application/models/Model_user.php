<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Model User
*/
class Model_user extends CI_Model
{
	public function login($data){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('u_id',$data['u_id']);
		$this->db->where('u_password',pwd_hash($data['u_password']));

		return $this->db->get();
	}
	public function select_all(){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->order_by('u_id','ASC');

		return $this->db->get();
	}
	public function select_by_id($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('u_id',$id);

		return $this->db->get();
	}
	public function reset($id){
		$data['u_password'] = pwd_hash($id);
		$this->db->where('u_id',$id);
		return $this->db->update('user',$data);
	}
	public function delete($id){
		return $this->db->delete('user',['u_id' => $id]);
	}
	public function insert($data){
		return $this->db->insert('user',$data);
	}
	public function update($id,$data){
		$this->db->where('u_id',$id);
		return $this->db->update('user',$data);
	}
	
}