<?php

/**
* My Controller
*/
class MY_Controller extends CI_Controller
{
	public $profile;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_user');
		if ($this->session->userdata('logged_in'))
		{
			$id 	= $this->session->userdata('username');
			$data 	= $this->model_user->select_by_id($id)->row();
			if (count($data)<0){
				show_error('Data User tidak ditemukan.','403');
				redirect('logout');
			}

			$this->profile = $data;
		}else{
			redirect('logout');
		}
	}

}