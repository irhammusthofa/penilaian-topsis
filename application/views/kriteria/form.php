<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?= $title ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?= $this->session->flashdata('notification') ?>
        <?php 
          $temp   = $this->session->flashdata('temp');
          $temp2  = $this->session->flashdata('temp2');
          if (!empty($temp2)){
            if (empty($temp)){
              $temp = $temp2;
            }
          }
        ?>
        <?= form_open($action."/".urlencode(@$data['kriteria']->kode),array('method'=>'post')) ?>
          <div class="box-body">
            <!--start-box-->
            <div class="row">
              <div class="col-md-6">
                
                <div class="form-group col-md-12">
                  <label>Kode <b style="color:red">*</b></label>
                  <input name="kode" type="text" class="form-control"  placeholder="Kode" value="<?= @$temp['kode'] ?>" required>
                </div>
                <div class="form-group col-md-12">
                  <label>Kriteria <b style="color:red">*</b></label>
                  <input name="kriteria" type="text" style="text-transform:uppercase" class="form-control"  placeholder="Masukan Kriteria" value="<?= @$temp['kriteria'] ?>" required>
                </div>
            </div>
            <!--end-box-->
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <?= anchor('kriteria','Batal',array('class'=> 'btn btn-default')) ?>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>