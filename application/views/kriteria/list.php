
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $title ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data Kriteria</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<?= $this->session->flashdata('notification'); ?>

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $title ?></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <table id="table1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>Kode</th>
            <th>Kriteria</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach (@$data['kriteria'] as $row) { ?>
            <tr>
              <td><?= $row->kode ?></td>
              <td><?= $row->kriteria ?></td>
              <td>
                <div class="input-group">
                  <button type="button" class="btn btn-default pull-right dropdown-toggle" data-toggle="dropdown">
                    <span> Action
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                  <ul class="dropdown-menu">
                    <li><?= anchor('kriteria/edit/'.urlencode($row->kode),'<i class="fa fa-edit"></i> Edit'); ?> </li>
                    <li><a href="#" id="btnHapus"  type="button" data-toggle="modal" onclick="btnHapus(<?= "'".urlencode($row->kode)."','".$row->kriteria."'" ?>)"><i class="fa fa-trash"></i> Hapus</a></li>
                  </ul>
                </div>
              </td>
            </tr>
          <?php } ?>
          
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <div class="form-group col-md-1">
        <?= anchor('kriteria/tambah/','<i class="fa fa-plus"></i> Tambah', array('class' => 'btn btn-primary'));?>
      </div>
    </div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->