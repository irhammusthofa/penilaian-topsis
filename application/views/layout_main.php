<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= @$title ?></title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url('assets/adminlte') ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/adminlte') ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte') ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte') ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte') ?>/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <?= (empty(@$assets['css'])) ? '' : convert_css(@$assets['css']) ?>
  <?= (empty(@$assets['js_header'])) ? '' : convert_js(@$assets['js_header'])  ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php create_modal(@$assets['modal']); ?>
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?= base_url('assets/adminlte') ?>/index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini" >ADM</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Panel</b> Admin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?= base_url('assets/adminlte') ?>/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= @$this->profile->u_name ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?= base_url('assets/adminlte') ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?= @$this->profile->u_name ?>
                  <small><?= role(@$this->profile->u_role) ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <!-- <div class="pull-left">
                  <?= anchor('akun/profil','Profil',array('class'=>'btn btn-default btn-flat')) ?>
                </div> -->
                <div class="pull-right">
                  <?= anchor('logout','Logout', array('class'=>'btn btn-default btn-flat')) ?>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url('assets/adminlte') ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= @$this->profile->u_name ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?= role(@$this->profile->u_role) ?></a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?= (@$active=='dasbor') ? 'active' : '' ?>">
          <?= anchor('dasbor','<i class="fa fa-dashboard"></i> <span>Dasbor</span>') ?>
        </li>
        <li class="<?= (@$active=='kriteria') ? 'active' : '' ?>">
          <?= anchor('kriteria','<i class="fa fa-calendar"></i> <span>Data Kriteria</span>') ?>
        </li>
        <li class="<?= (@$active=='karyawan') ? 'active' : '' ?>">
          <?= anchor('karyawan','<i class="fa fa-users"></i> <span>Data Karyawan</span>') ?>
        </li>
        <li class="<?= (@$active=='penilaian-topsis') ? 'active' : '' ?>">
          <?= anchor('penilaian/topsis','<i class="fa fa-calendar"></i> <span>Penilaian Topsis</span>') ?>
        </li>
        <!-- <li class="<?= (@$active=='laporan') ? 'active' : '' ?>">
          <?= anchor('laporan','<i class="fa fa-calendar"></i> <span>Laporan</span>') ?>
        </li> -->
        <?php if(@$this->profile->u_role=='ADMIN') { ?>
        <li class="<?= (@$active=='user') ? 'active' : '' ?>">
          <?= anchor('user','<i class="fa fa-user"></i> <span>User</span>') ?>
        </li>
        <?php } ?> 
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <?php (empty(@$content) ? '' : $this->load->view($content)) ?>
  </div>
  <!-- /.content-wrapper -->


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018.</strong> All rights
    reserved.
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?= base_url('assets/adminlte') ?>/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets/adminlte') ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url('assets/adminlte') ?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/adminlte') ?>/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/adminlte') ?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/adminlte') ?>/dist/js/demo.js"></script>
<script src="<?= base_url('node_modules/socket.io/node_modules/socket.io-client/socket.io.js') ?>"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
  <?= (empty(@$assets['js_footer'])) ? '' : convert_js(@$assets['js_footer']) ?>
<script async defer
    src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA4NCxFCTb30FQIwLk9Sg-1FSqtf6mZb34&callback=initMap">
    </script>
</body>
</html>
