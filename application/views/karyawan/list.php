
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= $title ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Karyawan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<?= $this->session->flashdata('notification'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?= $title ?></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <table id="table1" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>NIK</th>
                <th>Nama Lengkap</th>
                <th>L/P</th>
                <th>Alamat</th>
                <th>Telp</th>
                <th>Email</th>
                <th>Jabatan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach (@$data['karyawan'] as $row) { ?>
                <tr>
                  <td><?= $row->nik ?></td>
                  <td><?= $row->nama ?></td>
                  <td><?= substr($row->kelamin,0,1) ?></td>
                  <td><?= $row->alamat ?></td>
                  <td><?= $row->telp ?></td>
                  <td><?= $row->email ?></td>
                  <td><?= $row->jabatan ?></td>
                  <td>
                    <div class="input-group">
                      <button type="button" class="btn btn-default pull-right dropdown-toggle" data-toggle="dropdown">
                        <span> Action
                        </span>
                        <i class="fa fa-caret-down"></i>
                      </button>
                      <ul class="dropdown-menu">
                        <li><?= anchor('karyawan/edit/'.urlencode($row->nik),'<i class="fa fa-edit"></i> Edit'); ?> </li>
                        <li><a href="#" id="btnHapus"  type="button" data-toggle="modal" onclick="btnHapus(<?= "'".urlencode($row->nik)."','".$row->nama."'" ?>)"><i class="fa fa-trash"></i> Hapus</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php } ?>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="form-group col-md-1">
            <?= anchor('karyawan/tambah/','<i class="fa fa-plus"></i> Tambah', array('class' => 'btn btn-primary'));?>
          </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->