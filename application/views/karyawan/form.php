<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?= $title ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?= $this->session->flashdata('notification') ?>
        <?php 
          $temp   = $this->session->flashdata('temp');
          $temp2  = $this->session->flashdata('temp2');
          if (!empty($temp2)){
            if (empty($temp)){
              $temp = $temp2;
            }
          }
        ?>
        <?= form_open_multipart($action."/".urlencode(@$data['karyawan']->nik),array('method'=>'post')) ?>
          <div class="box-body">
            <!--start-box-->
            <div class="row">
              <div class="col-md-6">
                
                <div class="form-group col-md-12">
                  <label>NIK <b style="color:red">*</b></label>
                  <input name="nik" type="text" class="form-control"  placeholder="NIK" value="<?= @$temp['nik'] ?>" required>
                </div>
                <div class="form-group col-md-12">
                  <label>Nama Lengkap <b style="color:red">*</b></label>
                  <input name="nama" type="text" style="text-transform:uppercase" class="form-control"  placeholder="Masukan Nama Lengkap" value="<?= @$temp['nama'] ?>" required>
                </div>
                <div class="form-group col-md-6">
                  <label >Jenis Kelamin <b style="color:red">*</b></label>
                  <?php 
                    $jk = array('Laki-laki' => 'Laki-laki','Perempuan' => 'Perempuan');
                    echo form_dropdown('jenis_kelamin',@$jk,@$temp['kelamin'],[ 'id' => 'jenis_kelamin','class' => 'form-control select2','required' => 'true']); ?>
                </div>
                <div class="form-group col-md-6">
                  <label class="control-label">Telp</label>
                  <div class="row row-space-10">
                      <div class="col-md-12 m-b-15">
                          <input name="telp" type="text" class="form-control" placeholder="No. Telp" value="<?= @$temp['telp'] ?>" />
                      </div>
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <label class="control-label">Email</label>
                  <div class="row row-space-10">
                      <div class="col-md-12 m-b-15">
                          <input name="email" type="email" class="form-control" placeholder="Email" value="<?= @$temp['email'] ?>" />
                      </div>
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <label class="control-label">Alamat <b style="color:red">*</b></label>
                  <div class="row row-space-10">
                      <div class="col-md-12 m-b-15">
                          <textarea name="alamat" type="text" style="text-transform:uppercase" class="form-control" placeholder="Alamat" rows="3" required><?= @$temp['alamat'] ?></textarea>
                      </div>
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <label class="control-label">Jabatan</label>
                  <div class="row row-space-10">
                      <div class="col-md-12 m-b-15">
                          <input name="jabatan" type="text" class="form-control" placeholder="Jabatan" value="<?= @$temp['jabatan'] ?>" />
                      </div>
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <label>Nickname <b style="color:red">*</b></label>
                  <input name="username" type="text" style="text-transform:uppercase" class="form-control"  placeholder="Masukan Nickname" value="<?= @$temp['username'] ?>" required>
                </div>
                <div class="form-group col-md-12">
                  <label>Password  <?= (empty($temp2)) ? '<b style="color:red">*</b>' : '(Kosongkan jika tidak ada perubahan)' ?></label>
                  <input name="password" type="password" style="text-transform:uppercase" class="form-control"  placeholder="Masukan Password" <?= (empty($temp2)) ? 'required' : '' ?>>
                </div>
                <div class="form-group col-md-12">
                  <label class="control-label">Icon Marker MAP <?= (empty($temp2)) ? '<b style="color:red">*</b>' : '(Kosongkan jika tidak ada perubahan)' ?></label>
                  <div class="row m-b-15">
                      <div class="col-md-12">
                          <input type="file" name="foto" <?= (empty($temp2)) ? 'required' : '' ?>>
                          <label style="color:red;margin:10px">Tipe File (*.jpg,*.png), Ukuran Maksimum : 1 MB</label>
                      </div>
                  </div>
                </div>
            </div>
            <!--end-box-->
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <?= anchor('karyawan','Batal',array('class'=> 'btn btn-default')) ?>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>