
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= $title ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Penilaian Topsis</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<?= $this->session->flashdata('notification'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?= $title ?></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
          <div class="col-md-12 pull-right">
            <?= form_open('penilaian/topsis',array('method' => 'get')) ?>
            <br>
            <div class="form-group col-md-2">
              <?php $current_m = date('m');
                if (!empty(@$_GET['bulan'])){
                  $current_m = $_GET['bulan'];
                }
               ?>
              <label>Bulan</label>
              <select class="form-control" name="bulan">
                <option value="01" <?= ($current_m=='01') ? 'selected="true"' : '' ?>>Januari</option>
                <option value="02" <?= ($current_m=='02') ? 'selected="true"' : '' ?>>Februari</option>
                <option value="03" <?= ($current_m=='03') ? 'selected="true"' : '' ?>>Maret</option>
                <option value="04" <?= ($current_m=='04') ? 'selected="true"' : '' ?>>April</option>
                <option value="05" <?= ($current_m=='05') ? 'selected="true"' : '' ?>>Mei</option>
                <option value="06" <?= ($current_m=='06') ? 'selected="true"' : '' ?>>Juni</option>
                <option value="07" <?= ($current_m=='07') ? 'selected="true"' : '' ?>>Juli</option>
                <option value="08" <?= ($current_m=='08') ? 'selected="true"' : '' ?>>Agustus</option>
                <option value="09" <?= ($current_m=='09') ? 'selected="true"' : '' ?>>September</option>
                <option value="10" <?= ($current_m=='10') ? 'selected="true"' : '' ?>>Oktober</option>
                <option value="11" <?= ($current_m=='11') ? 'selected="true"' : '' ?>>November</option>
                <option value="12" <?= ($current_m=='12') ? 'selected="true"' : '' ?>>Desember</option>
              </select>
            </div>
            <div class="form-group col-md-1">
              <label>Tahun</label>
              <select class="form-control" name="tahun">
                <?php
                  $current_y = date('Y');
                  if (!empty(@$_GET['tahun'])) $current_y = $_GET['tahun'];
                  for($i=1;$i<300;$i++) { ?>
                  <option value="<?= $i+1900 ?>" <?= ($current_y==$i+1900) ? 'selected="true"' : '' ?>><?= $i+1900 ?></option>
                <?php } ?>
              </select>
            </div>
            <br>
            <button class="btn btn-primary" type="submit">Tampilkan</button>
            <?= form_close() ?>
          </div>
        </div>
        <div class="box-body">
          
          <table id="table1" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Id</th>
                <th>Tanggal</th>
                <th>Keterangan</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach (@$data['laporan'] as $row) { ?>
                <tr>
                  <td><?= $row->id ?></td>
                  <td><?= $row->tgl ?></td>
                  <td><?= $row->keterangan ?></td>
                  <td><?= $row->status ?></td>
                  <td>
                    <div class="input-group">
                      <button type="button" class="btn btn-default pull-right dropdown-toggle" data-toggle="dropdown">
                        <span> Action
                        </span>
                        <i class="fa fa-caret-down"></i>
                      </button>
                      <ul class="dropdown-menu">
                        <li><a href="#" id="btnHapus"  type="button" data-toggle="modal" onclick="btnHapus(<?= "'".urlencode($row->id)."'" ?>)"><i class="fa fa-trash"></i> Hapus</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php } ?>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="form-group col-md-1">
            <?= anchor('laporan/tambah/','<i class="fa fa-plus"></i> Pengajuan Laporan', array('class' => 'btn btn-primary'));?>
          </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->