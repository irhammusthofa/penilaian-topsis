<button id="btnCetak" onclick="cetak()">Cetak</button>

<h1 style="text-align: center;">Laporan Topsis</h1>
<h3 style="text-align: center; margin-top: -15px">Periode <?= @$bln." ".@$thn ?></h3>
  <!-- Main content -->
  <section class="content">
<?= $this->session->flashdata('notification'); ?>

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Kriteria</h3>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover" border="1" width="100%">
          <thead>
            <tr>
              <th>Kriteria</th>
              <?php foreach (@$data['kriteria-reference'] as $row) { ?>
                  <th><?= @$row['kode'] ?></th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Kepentingan atau Bobot</td>
              <?php foreach (@$data['kriteria-reference'] as $row) { ?>
                  <th><?= round(@$row['data']['bobot'],4) ?></th>
              <?php } ?>
            </tr>
            <tr>
              <td>Cost/Benefit</td>
              <?php foreach (@$data['kriteria-reference'] as $row) { ?>
                  <th><?= @$row['data']['c/b'] ?></th>
              <?php } ?>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Nilai Alternatif</h3>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover" border="1" width="100%">
          <thead>
            <tr>
              <th>Alternatif/Kriteria</th>
              <?php foreach (@$data['kriteria-reference'] as $row) { ?>
              <th><?= $row['kode'] ?></th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php foreach (@$data['karyawan'] as $row) { ?>
              <tr>
                <td><?= $row->nik ?></td>
                <?php foreach (@$data['kriteria-reference'] as $kriteria) { ?>
                <th><?= $this->model_nilai->rata2_kriteria($row->nik,$selected['bln'],$selected['thn'],$kriteria['kode']) ?></th>
                <?php } ?>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Keputusan Ternormalisasi</h3>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover" border="1"  width="100%">
          <thead>
            <tr>
              <th>Kriteria</th>
              <?php foreach (@$data['kriteria-reference'] as $row) { ?>
              <th><?= $row['kode'] ?></th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php foreach (@$data['karyawan'] as $karyawan) { ?>
              <tr>
                <td><?= $karyawan->nik ?></td>
                <?php foreach (@$data['kriteria-reference'] as $kriteria) { ?>
                  <?php foreach (@$data['ternormalisasi'] as $row) {
                    if($row['id_karyawan']==$karyawan->nik && $row['kriteria']==$kriteria['kode']){ 
                  ?>
                      <td><?= round($row['ternormalisasi'],4) ?></td>
                  <?php } } ?>
                <?php  } ?>
              </tr>
            <?php } ?>
            
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Keputusan Ternormalisasi dan Terbobot</h3>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover" border="1"  width="100%">
          <thead>
            <tr>
              <th>Kriteria</th>
              <?php foreach (@$data['kriteria-reference'] as $row) { ?>
              <th><?= $row['kode'] ?></th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php foreach (@$data['karyawan'] as $karyawan) { ?>
              <tr>
                <td><?= $karyawan->nik ?></td>
                <?php foreach (@$data['kriteria-reference'] as $kriteria) { ?>
                  <?php foreach (@$data['ternormalisasi'] as $row) {
                    if($row['id_karyawan']==$karyawan->nik && $row['kriteria']==$kriteria['kode']){ 
                  ?>
                      <td><?= round($row['terbobot'],4) ?></td>
                  <?php } } ?>
                <?php  } ?>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">HASIL PERHITUNGAN SOLUSI IDEAL POSITIF DAN SOLUSI IDEAL NEGATIF</h3>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover" border="1"  width="100%">
          <thead>
            <tr>
              <th>Kriteria</th>
              <?php foreach (@$data['ideal'] as $row) { ?>
                  <th><?= @$row['kriteria'] ?></th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>A+</td>
              <?php foreach (@$data['ideal'] as $row) { ?>
                  <td><?= round(@$row['a_plus'],4) ?></td>
              <?php } ?>
            </tr>
            <tr>
              <td>A-</td>
              <?php foreach (@$data['ideal'] as $row) { ?>
                  <td><?= round(@$row['a_min'],4) ?></td>
              <?php } ?>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">HASIL PERHITUNGAN KEDEKATAN RELATIF (RELATIVE CLOSENESS)</h3>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover" border="1"  width="100%">
          <thead>
            <tr>
              <th>Alternatif/Kriteria</th>
              <th>S+</th>
              <th>S-</th>
              <th>RC</th>
              <th>Rangking</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach (@$data['karyawan'] as $karyawan) { ?>
              <tr>
                <td><?= $karyawan->nik ?></td>
                <td><?= round($this->model_nilai->relative_closeness($karyawan->nik,$selected['bln'],$selected['thn'])['s_plus'],4) ?></td>
                <td><?= round($this->model_nilai->relative_closeness($karyawan->nik,$selected['bln'],$selected['thn'])['s_min'],4) ?></td>
                <td><?= round($this->model_nilai->relative_closeness($karyawan->nik,$selected['bln'],$selected['thn'])['rc'],4) ?></td>
                <td><?= $this->model_nilai->rangking($karyawan->nik,$selected['bln'],$selected['thn']) ?></td>
              </tr>
            <?php } ?>
            
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->

<script type="text/javascript">
  function cetak(){
    document.getElementById("btnCetak").style.visibility = "hidden";
    window.print();
    document.getElementById("btnCetak").style.visibility = "visible";
  }
</script>
