
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><?= anchor('penilaian/topsis'.@$redirect,'Topsis')?></li>
      <li class="active">Detail</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
<?= $this->session->flashdata('notification'); ?>

    <!-- Default box -->
    <div class="row">
      <div class="col-md-12">
        <div class="form-group col-md-6">
          <label>Karyawan : </label>
          <input type="text" name="karyawan" class="form-control" value="<?= $data['karyawan']->nik.' - '.$data['karyawan']->nama ?>" disabled/>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Kriteria</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Kriteria</th>
              <?php foreach (@$data['kriteria-reference'] as $row) { ?>
                  <th><?= @$row['kode'] ?></th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Kepentingan atau Bobot</td>
              <?php foreach (@$data['kriteria-reference'] as $row) { ?>
                  <th><?= @$row['data']['bobot'] ?></th>
              <?php } ?>
            </tr>
            <tr>
              <td>Cost/Benefit</td>
              <?php foreach (@$data['kriteria-reference'] as $row) { ?>
                  <th><?= @$row['data']['c/b'] ?></th>
              <?php } ?>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Nilai Alternatif</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Kriteria</th>
              <th>Nilai</th>
              <th>Pembagi</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach (@$data['kriteria-reference'] as $row) { ?>
              <tr>
                <td><?= $row['kode'] ?></td>
                <td><?= $this->model_nilai->rata2_kriteria($selected['nik'],$selected['bln'],$selected['thn'],$row['kode']) ?></td>
                <td><?= $row['data']['pembagi'] ?></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Keputusan Ternormalisasi</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Kriteria</th>
              <th>Nilai</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach (@$data['ternormalisasi'] as $row) {
              if($row['id_karyawan']==$selected['nik']){ 
            ?>
              <tr>
                <td><?= $row['kriteria'] ?></td>
                <td><?= $row['ternormalisasi'] ?></td>
              </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Keputusan Ternormalisasi dan Terbobot</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Kriteria</th>
              <th>Nilai</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach (@$data['ternormalisasi'] as $row) {
              if($row['id_karyawan']==$selected['nik']){ 
            ?>
              <tr>
                <td><?= $row['kriteria'] ?></td>
                <td><?= $row['terbobot'] ?></td>
              </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">HASIL PERHITUNGAN SOLUSI IDEAL POSITIF DAN SOLUSI IDEAL NEGATIF</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Kriteria</th>
              <?php foreach (@$data['ideal'] as $row) { ?>
                  <th><?= @$row['kriteria'] ?></th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>A+</td>
              <?php foreach (@$data['ideal'] as $row) { ?>
                  <th><?= @$row['a_plus'] ?></th>
              <?php } ?>
            </tr>
            <tr>
              <td>A-</td>
              <?php foreach (@$data['ideal'] as $row) { ?>
                  <th><?= @$row['a_min'] ?></th>
              <?php } ?>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">HASIL PERHITUNGAN KEDEKATAN RELATIF (RELATIVE CLOSENESS)</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <table id="table1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Deskripsi</th>
              <th>Nilai</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>S+</td>
              <td><?= round($this->model_nilai->relative_closeness($selected['nik'],$selected['bln'],$selected['thn'])['s_plus'],4) ?></td>
            </tr>
            <tr>
              <td>S-</td>
              <td><?= round($this->model_nilai->relative_closeness($selected['nik'],$selected['bln'],$selected['thn'])['s_min'],4) ?></td>
            </tr>
            <tr>
              <td>RC</td>
              <td><?= round($this->model_nilai->relative_closeness($selected['nik'],$selected['bln'],$selected['thn'])['rc'],4) ?></td>
            </tr>
            <tr>
              <td>Rangking</td>
              <td><?= $this->model_nilai->rangking($selected['nik'],$selected['bln'],$selected['thn']) ?></td>
            </tr>
            
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->