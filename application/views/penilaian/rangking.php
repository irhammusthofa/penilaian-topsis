<button id="btnCetak" onclick="cetak()">Cetak</button>

<h1 style="text-align: center;">Laporan Rangking Karyawan</h1>
<h3 style="text-align: center; margin-top: -15px">Periode <?= @$bln." ".@$thn ?></h3>
  <!-- Main content -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Rangking Karyawan</h3>
  </div>
  <div class="box-body">
    <table id="table1" class="table table-bordered table-hover" border="1"  width="100%">
      <thead>
        <tr>
          <th>Rangking</th>
          <th>NIK</th>
          <th>Nama</th>
          <th>RC</th>
        </tr>
      </thead>
      <tbody>
        <?php $tertinggi = ""; $rendah = 0; foreach (@$data['karyawan'] as $karyawan) { 
          $rangking = $this->model_nilai->rangking($karyawan->nik,$selected['bln'],$selected['thn']);
          if ($rangking==1){
            $tertinggi = $karyawan->nik." - ".$karyawan->nama;
          }
          if ($rendah<$rangking){
            $rendah = $rangking;
            $terendah = $karyawan->nik." - ".$karyawan->nama;
          }
          ?>
          <tr>
            <td><?= $rangking; ?></td>
            <td><?= $karyawan->nik ?></td>
            <td><?= $karyawan->nama ?></td>
            <td><?= round($this->model_nilai->relative_closeness($karyawan->nik,$selected['bln'],$selected['thn'])['rc'],4) ?></td>
            
          </tr>
        <?php } ?>
        
      </tbody>
    </table>
  </div>
  <!-- /.box-body -->
  <!-- /.box-footer-->
</div>

<p>Karyawan dengan terbaik adalah <b><?= $tertinggi ?></b></p>
<p style="margin-top: -15px">Karyawan dengan nilai terendah adalah <b><?= $terendah ?></b></p>

<table width="100%">
    <tr>
      <td width="70%">
      </td>
      <td width="30%">
        <p style="margin-bottom: 50px">Mengetahui,</p>
        <p>Kepala Koperasi Telkom</p>
      </td>
    </tr>
</table>

<script type="text/javascript">
  function cetak(){
    document.getElementById("btnCetak").style.visibility = "hidden";
    window.print();
    document.getElementById("btnCetak").style.visibility = "visible";
  }
</script>