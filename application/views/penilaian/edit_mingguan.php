
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><?= anchor('penilaian/topsis'.@$redirect,'Topsis')?></li>
      <li class="active">Edit</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
<?= $this->session->flashdata('notification'); ?>

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><?= $title ?></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label>Karyawan : </label>
              <input type="text" name="karyawan" class="form-control" value="<?= $data['karyawan']->nik.' - '.$data['karyawan']->nama ?>" disabled/>
            </div>
          </div>
          <div class="col-md-12">  
            <?= form_open('penilaian/edit_mingguan/'.$selected['nik'].'/'.$selected['bln'].'/'.$selected['thn'],array('method'=>'get')) ?>
            <div class="form-group col-md-4">
              <label>Minggu ke</label>
              <?= form_dropdown('minggu',$data['minggu'],@$selected['minggu'],[ 'id'=>'minggu', 'class' => 'form-control','onchange'=>'submit()',]); ?>
            </div>
            <div class="form-group col-md-4">
              <label>Kriteria</label>
              <?= form_dropdown('kriteria',$data['kriteria'],@$selected['kriteria'],[ 'id'=>'kriteria', 'class' => 'form-control','onchange'=>'submit()',]); ?>
            </div>
            <div class="form-group col-md-3">
              <label>Rata-rata : </label>
              <input type="text" name="rata2" class="form-control" value="<?= $data['rata2'] ?>" disabled/>
            </div>
            <?= form_close()?>
          </div>
        </div>
        <?= form_open('penilaian/simpan/'.$selected['nik'].'/'.$selected['bln'].'/'.$selected['thn'].'/'.@$selected['kriteria'].'/'.$selected['minggu'].'/mingguan',array('method'=>'post')) ?>
        <table id="table1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sub Kriteria</th>
              <th>Nilai</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach (@$data['sub_kriteria'] as $row) {
              $nilai = @$this->model_nilai->get_nilai_nik_sub($selected['nik'],$row->id,$selected['minggu'])->row()->nilai;
            ?>
              <tr>
                <td><?= @$row->sub_kriteria ?></td>
                <td><input name="nilai[<?= $row->id ?>]" type="text" class="form-control" value="<?= @$nilai ?>" /></td>
              </tr>
            <?php } ?>
            
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="form-group col-md-1">
          <button class="btn btn-primary" type="submit">Simpan</button>
        </div>
      </div>
      <!-- /.box-footer-->
      <?= form_close() ?>
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->