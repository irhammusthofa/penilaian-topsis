/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100130
 Source Host           : localhost:3306
 Source Schema         : penilaian_kk

 Target Server Type    : MySQL
 Target Server Version : 100130
 File Encoding         : 65001

 Date: 03/05/2018 09:26:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for karyawan
-- ----------------------------
DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE `karyawan` (
  `nik` varchar(16) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `kelamin` varchar(10) NOT NULL,
  `icon_map` varchar(50) DEFAULT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `token` varchar(50) NOT NULL,
  PRIMARY KEY (`nik`),
  UNIQUE KEY `k_token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of karyawan
-- ----------------------------
BEGIN;
INSERT INTO `karyawan` VALUES ('1', 'Eko Nurkholis', 'Bandung JAWA BARAT', '08132772821', 'admin@igpkhijabar.com', 'Karyawan Swasta', 'Perempuan', NULL, '1', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '1');
INSERT INTO `karyawan` VALUES ('2', 'Santi Anita', NULL, NULL, NULL, NULL, '', NULL, '1', '', '2');
INSERT INTO `karyawan` VALUES ('3', 'Nasihah Ulya', NULL, NULL, NULL, NULL, '', NULL, '', '', '3');
INSERT INTO `karyawan` VALUES ('4', 'Dhenis Saputra', NULL, NULL, NULL, NULL, '', NULL, '', '', '4');
INSERT INTO `karyawan` VALUES ('5', 'Win Ariayoda', NULL, NULL, NULL, NULL, '', NULL, '', '', '5');
COMMIT;

-- ----------------------------
-- Table structure for kriteria
-- ----------------------------
DROP TABLE IF EXISTS `kriteria`;
CREATE TABLE `kriteria` (
  `kode` varchar(5) NOT NULL,
  `kriteria` varchar(50) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kriteria
-- ----------------------------
BEGIN;
INSERT INTO `kriteria` VALUES ('C1', 'Kerja Sama');
INSERT INTO `kriteria` VALUES ('C2', 'Kehadiran');
INSERT INTO `kriteria` VALUES ('C3', 'Tanggung jawab dalam pekerjaan');
INSERT INTO `kriteria` VALUES ('C4', 'Kedisiplinan');
INSERT INTO `kriteria` VALUES ('C5', 'Kejujuran');
COMMIT;

-- ----------------------------
-- Table structure for penilaian
-- ----------------------------
DROP TABLE IF EXISTS `penilaian`;
CREATE TABLE `penilaian` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_karyawan` varchar(16) NOT NULL,
  `kriteria` varchar(5) NOT NULL,
  `nilai` int(3) NOT NULL,
  `jenis` int(1) DEFAULT '0' COMMENT '0 = Topsis, 1-4 = minggu',
  `bln_thn` varchar(7) NOT NULL COMMENT 'Bulan dan Tahun',
  PRIMARY KEY (`id`),
  KEY `fk_karyawan` (`id_karyawan`),
  KEY `fk_kriteria` (`kriteria`),
  CONSTRAINT `fk_karyawan` FOREIGN KEY (`id_karyawan`) REFERENCES `karyawan` (`nik`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_kriteria` FOREIGN KEY (`kriteria`) REFERENCES `kriteria` (`kode`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of penilaian
-- ----------------------------
BEGIN;
INSERT INTO `penilaian` VALUES (206, '1', 'C1', 2, 0, '05-2018');
INSERT INTO `penilaian` VALUES (207, '1', 'C2', 3, 0, '05-2018');
INSERT INTO `penilaian` VALUES (208, '1', 'C3', 1, 0, '05-2018');
INSERT INTO `penilaian` VALUES (209, '1', 'C4', 4, 0, '05-2018');
INSERT INTO `penilaian` VALUES (210, '1', 'C5', 5, 0, '05-2018');
INSERT INTO `penilaian` VALUES (211, '2', 'C1', 3, 0, '05-2018');
INSERT INTO `penilaian` VALUES (212, '2', 'C2', 4, 0, '05-2018');
INSERT INTO `penilaian` VALUES (213, '2', 'C3', 5, 0, '05-2018');
INSERT INTO `penilaian` VALUES (214, '2', 'C4', 5, 0, '05-2018');
INSERT INTO `penilaian` VALUES (215, '2', 'C5', 1, 0, '05-2018');
INSERT INTO `penilaian` VALUES (216, '3', 'C1', 5, 0, '05-2018');
INSERT INTO `penilaian` VALUES (217, '3', 'C2', 5, 0, '05-2018');
INSERT INTO `penilaian` VALUES (218, '3', 'C3', 4, 0, '05-2018');
INSERT INTO `penilaian` VALUES (219, '3', 'C4', 1, 0, '05-2018');
INSERT INTO `penilaian` VALUES (220, '3', 'C5', 2, 0, '05-2018');
INSERT INTO `penilaian` VALUES (221, '4', 'C1', 2, 0, '05-2018');
INSERT INTO `penilaian` VALUES (222, '4', 'C2', 3, 0, '05-2018');
INSERT INTO `penilaian` VALUES (223, '4', 'C3', 4, 0, '05-2018');
INSERT INTO `penilaian` VALUES (224, '4', 'C4', 4, 0, '05-2018');
INSERT INTO `penilaian` VALUES (225, '4', 'C5', 4, 0, '05-2018');
INSERT INTO `penilaian` VALUES (226, '5', 'C1', 4, 0, '05-2018');
INSERT INTO `penilaian` VALUES (227, '5', 'C2', 4, 0, '05-2018');
INSERT INTO `penilaian` VALUES (228, '5', 'C3', 5, 0, '05-2018');
INSERT INTO `penilaian` VALUES (229, '5', 'C4', 3, 0, '05-2018');
INSERT INTO `penilaian` VALUES (230, '5', 'C5', 3, 0, '05-2018');
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `u_id` varchar(10) NOT NULL,
  `u_name` varchar(50) NOT NULL,
  `u_password` varchar(50) NOT NULL,
  `u_email` varchar(30) NOT NULL,
  `u_status` int(1) NOT NULL,
  `u_role` int(1) NOT NULL,
  PRIMARY KEY (`u_id`) USING BTREE,
  UNIQUE KEY `u_email` (`u_email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('admin', 'Administrator', '1b9fe1bbf7dec514ffe2cc245f2dd94adf882f5a', 'irhammusthofa@gmail.com', 1, 1);
INSERT INTO `user` VALUES ('pimpinan', 'Pimpinan', '1b9fe1bbf7dec514ffe2cc245f2dd94adf882f5a', '', 1, 2);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
